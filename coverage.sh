#!/bin/bash
function abort {
  echo "${*}" \
    && exit 1 \
    || exit 1
}

function main {
    local coverage
    local min_coverage=100
    local msg="Coverage is ${*}% which is less than 100%"

    for d in */ ; do
        pushd "${d}" \
            && echo "[INFO] Checking Coverage report for ${d}" \
            && coverage="$(
                go test -cover \
                | grep -Eo 'coverage: [0-9]{1,4}' \
                | grep -Eo '[0-9]{1,4}' \
            )" \
        && popd \
        && if test "${coverage}" -lt "${min_coverage}"; then
            abort "[ERROR] Coverage is ${coverage}% on ${d} which is less than ${min_coverage}"
        fi
    done
}

main "${@}"
