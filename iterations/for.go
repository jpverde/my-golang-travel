package main

import (
	"strings"
)

// This function takes a text and repeats it a number of times
func Repeat(text string, quant int) (repeated string) {
	repeated = strings.Repeat(text, quant)
	return
}

func main() {}
